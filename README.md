# Mixed Messages

#### Made by *Zishan Ahmed*

## Table of contents
 - General information
 - Technologies
 - Launch
 - Project status
 - Sources
 - Other Information

## General Information
The aim of this project is to generate a new random message whenever the user runs the program. This project is part of the "Full-Stack Engineer" course over on Codecademy, and is a mean for us to showcase our learning of JavaScript thus far.
 
## Technologies
The Mixed Messages project will be written using JavaScript ES6.

## Launch
A command-line application called 'Node' will be required to run the JavaScript file.

## Project status
The project has now been completed

## Sources
Codecademy has been the main source to get information on how to use objects, arrays and functions. Some of the phrases used in random phrase were taken from the following article: https://parade.com/966564/parade/fun-facts/

## Other Information
This project has only been to showcase the functionality I have learned so far in JavaScript. The phrases in each array within the object is fairly limited, but it can be added upon to make the program more diverse. 
