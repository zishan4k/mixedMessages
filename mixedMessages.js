//Create an object that will hold the 3 arrays containing different elements to make up the random phrase

let randomPhraseElement = {
  phrase1: ['Hi!', 'Hello there!', 'Good Morning.', 'How\'s it going?'],
  phrase2: ['Did you know, ', 'As the adage says, ', 'When I was a kid, my father told me ', 'I read an article online that said, '],
  phrase3: ['the original name for Google was Backrub.', 'bats are the only mammal that can actually fly.', 'elephants can\'t jump.', 'You give a poor man a fish, and you feed him for a day... You teach him to ahh.. fish, you give him... you give him.. ahh no, no...', 'don\'t cover the judge by... don\'t judge the cover, judge the book... aye man... don\'t.. don\'t book the cover, book... aye, Whatever, don\'t do it, just don\'t do it!']
}

//Assign a random index number to each phrase using the Math module

let phrase1In = Math.floor(Math.random() * 4)
let phrase2In = Math.floor(Math.random() * 4)
let phrase3In = Math.floor(Math.random() * 5)

//console.log(phrase1In)
//console.log(phrase2In)
//console.log(phrase3In)

// Write a function that will use the random index number along with the random phrase elemetns to generate a full random phrase

let randomPhrase = () => {
  console.log(`${randomPhraseElement.phrase1[phrase1In]} ${randomPhraseElement.phrase2[phrase2In]} ${randomPhraseElement.phrase3[phrase3In]}`)
}

// Call the function to display the random phrase when program is run
randomPhrase ();
